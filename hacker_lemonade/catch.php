<?php
   //File will be rewritten if already exists
   function write_file($filename,$newdata) {
          $f=fopen($filename,"w") or die("Couldn't open $filename for writing!"); 
          fwrite($f,$newdata);
          fclose($f);  
   }

   function append_file($filename,$newdata) {
          $f=fopen($filename,"a") or die("Couldn't open $filename for writing!"); 
          fwrite($f,$newdata);
          fclose($f);  
   }

   function read_file($filename) {
          $f=fopen($filename,"r") or die("Couldn't open $filename for writing!"); 
		  $fsize=filesize($filename);
		  if (!$fsize) return "[nothing in the file]";
		  $data=fread($f,$fsize);
          fclose($f);  
          return $data;
   }
   
	function buildgetstring() {
		$i=0; $str="";
		foreach ($_GET as $key => $value)
		{
			if ($i++>0) $str .="; ";
			$str .= $key . '=' . $value;
		}
	  return $str;
	}
   
  $file = "data_saved.txt";
  $save_str = buildgetstring();    

  if($save_str) {
	if (isset($_REQUEST["reset"])) {
		echo "<b>Reset done.</b><br/>"; 
		write_file($file, "");
		die;
	} else {
		echo "<b>Going to save: </b>" . htmlentities($save_str) . "<br/>"; 
		$save_str = $save_str . "[newline]";    
		//add more data ot the existing file
		if (filesize($file) > 3000) write_file($file, $save_str);
		else append_file($file, $save_str);
	}
  }

  $data = read_file($file);
  $disp_data = str_replace("[newline]", "<br/>", htmlentities($data));
  echo "<h2>Previously received values: </h2>" . $disp_data . "<br/>"; 
?> 